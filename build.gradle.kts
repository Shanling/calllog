plugins {
    kotlin("kapt") version Vof.kotlin apply false
}

buildscript {
    repositories {
        google()
        jcenter()
    }
    dependencies {
        classpath("com.android.tools.build:gradle:${Vof.androidBuildTools}")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${Vof.kotlin}")
    }
}

allprojects {
    repositories {
        google()
        jcenter()
    }
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}