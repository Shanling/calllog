**What are the libraries or frameworks you have chosen? and why?**

I didn't need to use anything outside of standard Jetpack technologies for this application. I use Kotlin Coroutines for making asynchronous list updates for my ListAdapter to observe and data binding/binding adapters to ensure seamless data/UI integration.


**If you had more time, what would you have done better?**

I didn't have time to make integration tests for the application. If I had time I would have mocked the cursor creation process (using Mockk library) and then tested the generation of data items from the cursor, as well as updating the LiveData list, in the very least. I have some features that I would have liked to add for a real call log application, such as contact pictures and the ability to place phone calls from within the application. This implementation is really like a < version 1.0 which just implements the core technology, but can be expanded on quite a bit to make a more useful application.

**What are some of the key architecture considerations?**

The only real thing to consider in a simple application like this is separation of concerns between the view model and the UI. By using LiveData and data binding keeping the data separate from the UI is easy, and by using binding adapters updating views is seamless. If I were to add something in the future such as server integration for loading call logs from other devices or something to that extent, I would need to add in a Room database and a repository class, but for just getting data from your local Android device, it's not necessary. In earlier commits you can see I was messing around with how I would set up the application if I were to expand on it later and add data from a remote server, or how I would go about adding contact images for known contacts.