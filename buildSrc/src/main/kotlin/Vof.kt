object Vof {
    //jetbrains
    const val kotlin = "1.3.31"

    //android
    const val androidBuildTools = "3.4.0"
    const val androidxAppCompat = "1.1.0-alpha05"
    const val androidx = "1.2.0-alpha01"
    const val constraintLayout = "2.0.0-beta1"
    const val androidxEspresso = "3.2.0-beta01"

    //androidx
    const val androidxNav = "2.1.0-alpha04"
    const val coroutines = "1.2.1"
    const val lifecycle = "2.2.0-alpha01"

    //testing
    const val junit = "4.12"
    const val androidxJunit = "1.1.2-alpha01"
}