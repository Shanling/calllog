plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-android-extensions")
    kotlin("kapt")
}

android {
    compileSdkVersion(29)
    defaultConfig {
        applicationId = "com.shan.calllog"
        minSdkVersion(21)
        targetSdkVersion(29)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

    dataBinding {
        isEnabled = true
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    //androidx
    implementation("androidx.appcompat:appcompat:${Vof.androidxAppCompat}")
    implementation("androidx.core:core-ktx:${Vof.androidx}")
    implementation("androidx.constraintlayout:constraintlayout:${Vof.constraintLayout}")
    implementation("androidx.navigation:navigation-fragment-ktx:${Vof.androidxNav}")
    implementation("androidx.navigation:navigation-ui-ktx:${Vof.androidxNav}")
    implementation("androidx.lifecycle:lifecycle-extensions:${Vof.lifecycle}")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:${Vof.lifecycle}")
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:${Vof.lifecycle}")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:${Vof.lifecycle}")
    //jetbrains
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Vof.kotlin}")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Vof.coroutines}")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:${Vof.coroutines}")
    implementation("org.jetbrains.kotlin:kotlin-stdlib:${Vof.kotlin}")
    testImplementation("org.jetbrains.kotlin:kotlin-reflect:${Vof.kotlin}")
    //testing
    testImplementation("junit:junit:${Vof.junit}")
    androidTestImplementation("androidx.test.ext:junit-ktx:${Vof.androidxJunit}")
    androidTestImplementation("androidx.test.espresso:espresso-core:${Vof.androidxEspresso}")
}
