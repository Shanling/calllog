package com.shan.calllog.home

import android.content.*
import android.database.*
import android.os.*
import android.provider.*
import android.view.*
import androidx.databinding.*
import androidx.fragment.app.*
import androidx.lifecycle.*
import com.shan.calllog.*
import com.shan.calllog.MainActivity.Companion.checkCallLogPermission
import com.shan.calllog.R
import com.shan.calllog.databinding.*
import kotlinx.coroutines.*

class HomeFrag : Fragment() {
    private lateinit var binding: HomeFragBinding
    private lateinit var viewModel: HomeFragVM

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.home_frag, container, false)

        viewModel = ViewModelProviders.of(this).get(HomeFragVM::class.java)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        val phoneLogAdapter = PhoneLogAdapter()
        binding.phoneLogRecyclerView.adapter = phoneLogAdapter

        viewModel.logList.observe(this) { phoneLogList ->
            phoneLogAdapter.submitList(phoneLogList)
        }

        //load new calls on swipe
        binding.swipeRefreshView.apply() {
            setOnRefreshListener {
                viewModel.launch(Dispatchers.IO) {
                    viewModel.updateLogList(makeCursor(context))
                    this@apply.isRefreshing = false
                }
            }
        }

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        //get new call log when app resumes
        //this takes care of call log updates when phone call is finished
        //as onResume() is called before focus is regained
        viewModel.launch(Dispatchers.IO) {
            viewModel.updateLogList(makeCursor(requireContext()))
        }
    }

    companion object {
        fun makeCursor(context: Context): Cursor? {
            if (checkCallLogPermission(context)) {
                return context.contentResolver.query(
                    CallLog.Calls.CONTENT_URI,
                    arrayOf(
                        CallLog.Calls.CACHED_NAME,
                        CallLog.Calls.NUMBER,
                        CallLog.Calls.TYPE,
                        CallLog.Calls.DATE
                    ),
                    null,
                    null,
                    null
                )
            } else {
                return null
            }
        }
    }
}