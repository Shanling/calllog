package com.shan.calllog.home

import android.database.*
import android.provider.*
import androidx.lifecycle.*
import com.shan.calllog.data.*
import kotlinx.coroutines.*
import kotlin.coroutines.*

class HomeFragVM : ViewModel(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = viewModelScope.coroutineContext

    private val _logList = MutableLiveData<List<PhoneLog>>(emptyList())
    val logList: LiveData<List<PhoneLog>> get() = _logList

    suspend fun updateLogList(cursor: Cursor?) {
        withContext(Dispatchers.Main) {
            _logList.value = getPhoneLogsFromDevice(cursor)
        }
    }

    private fun getPhoneLogsFromDevice(cursor: Cursor?): MutableList<PhoneLog> {
        val tempLogList = mutableListOf<PhoneLog>()

        //iterate through log
        cursor?.let {
            while (it.moveToNext()) {
                tempLogList += PhoneLog(
                    date = it.getLong(3),
                    phoneNumber = it.getString(1),
                    callType = it.getInt(2)
                )
            }

            it.close()
        }

        tempLogList.sortByDescending {
            it.date
        }

        //only have the most recent 50 calls
        return if (tempLogList.size > 50) {
            tempLogList.subList(0, 50)
        } else {
            tempLogList
        }
    }

    override fun onCleared() {
        super.onCleared()
        cancel()
    }
}