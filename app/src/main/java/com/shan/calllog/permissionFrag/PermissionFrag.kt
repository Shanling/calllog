package com.shan.calllog.permissionFrag

import android.*
import android.app.*
import android.content.pm.*
import android.os.*
import android.view.*
import android.widget.*
import androidx.core.content.*
import androidx.databinding.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.*
import com.shan.calllog.*
import com.shan.calllog.MainActivity.Companion.checkCallLogPermission
import com.shan.calllog.R
import com.shan.calllog.databinding.*

class PermissionFrag : Fragment() {

    private lateinit var binding: PermissionFragBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.permission_frag, container, false)
        binding.lifecycleOwner = this

        binding.callLogPermissionButton.setOnClickListener {
            requestCallLogPermission(activity)
        }

        return binding.root
    }

    private fun requestCallLogPermission(activity: Activity?) {
        if (activity != null) {
            //handle mid-app removing of permissions
            if (checkCallLogPermission(activity.applicationContext)) {
                //permission not granted
                requestPermissions(
                    arrayOf(Manifest.permission.READ_CALL_LOG),
                    READ_CALL_LOG_PERMISSION_CODE
                )
            } else {
                //permission has already been granted, navigate immediately to next fragment
                findNavController().navigate(R.id.action_permissionFrag_to_homeFrag)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            READ_CALL_LOG_PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    findNavController().navigate(R.id.action_permissionFrag_to_homeFrag)
                } else {
                    Toast.makeText(
                        context,
                        "You must enable the call log permission to use Call Log.",
                        Toast.LENGTH_LONG
                    ).show()
                }
                return
            }
        }
    }

    companion object {
        const val READ_CALL_LOG_PERMISSION_CODE = 42
    }

}
