package com.shan.calllog.data

data class PhoneLog(
    val date : Long,
    val phoneNumber : String,
    val callType: Int
)