package com.shan.calllog

import android.content.*
import android.graphics.*
import android.provider.*
import android.telephony.*
import android.widget.*
import androidx.databinding.*
import com.shan.calllog.data.*
import java.io.*
import java.text.*
import java.util.*

@BindingAdapter("phoneNumberFormatted")
fun TextView.setPhoneNumberFormatted(phoneLog: PhoneLog) {
    text = formatPhoneNumber(phoneLog.phoneNumber)
}

fun formatPhoneNumber(phoneNumber: String): String {
    return PhoneNumberUtils.formatNumber(phoneNumber, Locale.getDefault().country)
}

@BindingAdapter("dateFormatted")
fun TextView.setDateFormatted(phoneLog: PhoneLog) {
    text = formatDate(phoneLog.date)
}

fun formatDate(date: Long): String {
    return SimpleDateFormat("HH:mm a  MM/dd/yy", Locale.getDefault()).format(date)
}

