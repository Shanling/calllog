package com.shan.calllog

import android.view.*
import androidx.recyclerview.widget.*
import com.shan.calllog.data.*
import com.shan.calllog.databinding.*

class PhoneLogAdapter : ListAdapter<PhoneLog, PhoneLogViewHolder>(PhoneLogDiffCallback()) {
    override fun onBindViewHolder(holder: PhoneLogViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhoneLogViewHolder {
        return PhoneLogViewHolder.from(parent)
    }
}

class PhoneLogViewHolder(private val binding: RecyclerItemBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: PhoneLog) {
        binding.incOutIconIV.setImageResource(when (item.callType) {
            1 -> R.drawable.ic_incoming_icon
            2 -> R.drawable.ic_outgoing_icon
            3 -> R.drawable.ic_missed_icon
            4 -> R.drawable.ic_voicemail_icon
            5 -> R.drawable.ic_rejection_icon
            6 -> R.drawable.ic_blocked_icon
            else -> R.drawable.ic_missed_icon
        })

        binding.phoneLog = item
        binding.executePendingBindings()
    }

    companion object {
        fun from(parent: ViewGroup): PhoneLogViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = RecyclerItemBinding.inflate(layoutInflater, parent, false)
            return PhoneLogViewHolder(binding)
        }
    }
}

class PhoneLogDiffCallback : DiffUtil.ItemCallback<PhoneLog>() {
    override fun areItemsTheSame(oldItem: PhoneLog, newItem: PhoneLog): Boolean {
        return oldItem.date == newItem.date
    }

    override fun areContentsTheSame(oldItem: PhoneLog, newItem: PhoneLog): Boolean {
        return oldItem == newItem
    }
}
