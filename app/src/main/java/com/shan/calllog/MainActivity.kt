package com.shan.calllog

import android.*
import android.content.*
import android.content.pm.*
import android.os.*
import androidx.appcompat.app.*
import androidx.core.content.*
import androidx.databinding.*
import androidx.navigation.*
import com.shan.calllog.databinding.*

class MainActivity : AppCompatActivity() {

    private val binding: ActivityMainBinding by lazy {
        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding

        val callLogPermissionCheck = checkCallLogPermission(applicationContext)

        if (callLogPermissionCheck) {
            println("navigating")
            this.findNavController(R.id.nav_host_frag).navigate(R.id.action_permissionFrag_to_homeFrag)
        }
    }

    companion object {
        fun checkCallLogPermission(context: Context): Boolean {
            return ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.READ_CALL_LOG
            ) == PackageManager.PERMISSION_GRANTED
        }
    }
}
